import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import { taskToggleHandler, addTaskHandler, inputChangeHandler } from "../actions/task.actions";

const Task = () => {
    // B3: Khai báo dispatch để đẩy action tới reducer
    const dispatch = useDispatch();

    // B1: Nhận giá trị khởi tạo của state trong giai đoạn mounting
    const { input, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    // B2: Khai báo những actions
    const inputTaskChangeHandler = (event) => {
        dispatch(inputChangeHandler(event.target.value));
    }

    const addClickHandler = () => {
        dispatch(addTaskHandler());
    }

    const toggleClickHandler = (elementId) => {
        dispatch(taskToggleHandler(elementId));
    }

    return (
        <Container>
            <Grid container mt={5} alignItems="center">
                <Grid item xs={12} md={6} lg={8} sm={12}>
                    <TextField label="Input task here" fullWidth value={input} onChange={inputTaskChangeHandler}/>
                </Grid>
                <Grid item xs={12} md={6} lg={4} sm={12} textAlign="center">
                    <Button variant="contained" onClick={addClickHandler}>Add task</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List>
                    {taskList.map((element, index) => {
                        // console.log(element)
                        // {id: 2, name: 'Task 2', status: true}
                        return <ListItem onClick={() => toggleClickHandler(element.id)} key={element.id} style={{color: element.status ? "green" : "red"}}>{index + 1}. {element.name}</ListItem>;
                    })}
                </List>
            </Grid> 
        </Container>
    )
}

export default Task;